// Fill out your copyright notice in the Description page of Project Settings.


#include "DoorTrigger.h"
#include "AzurakCharacter.h"

ADoorTrigger::ADoorTrigger()
{
    OnActorBeginOverlap.AddDynamic(this, &ADoorTrigger::Event);
}

void ADoorTrigger::BeginPlay()
{
    Super::BeginPlay();
}

void ADoorTrigger::Event(class AActor* overlappedActor, class AActor* otherActor)
{
    Character = otherActor;
    if (Door) {
        Door->Character = Character;
        Door->OnInteract();
    }
}