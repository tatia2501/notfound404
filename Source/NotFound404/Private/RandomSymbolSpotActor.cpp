// Fill out your copyright notice in the Description page of Project Settings.


#include "RandomSymbolSpotActor.h"

// Sets default values
ARandomSymbolSpotActor::ARandomSymbolSpotActor()
{
    SpotMeshComponent = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("CubeMesh"));
    static ConstructorHelpers::FObjectFinder<UStaticMesh> CubeMeshAsset(TEXT("StaticMesh'/Engine/BasicShapes/Cube.Cube'"));
    if (CubeMeshAsset.Succeeded())
    {
        SpotMeshComponent->SetStaticMesh(CubeMeshAsset.Object);
    }
    SpotMeshComponent->SetupAttachment(RootComponent);
}

void ARandomSymbolSpotActor::ApplyMaterial(UMaterialInterface* Material)
{
    if (SpotMeshComponent)
    {
        SpotMeshComponent->SetMaterial(0, Material);
    }
}