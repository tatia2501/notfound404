// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "BehaviorTree/BehaviorTree.h"
#include "Components/AudioComponent.h"
#include "GameFramework/Character.h"
#include "PatrolPath.h"
#include "AzurakCharacter.generated.h"

UCLASS()
class NOTFOUND404_API AAzurakCharacter : public ACharacter
{
	GENERATED_BODY()

public:
	// Sets default values for this character's properties
	AAzurakCharacter();

	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	UBehaviorTree* GetBehavoirTree() const;
	APatrolPath* GetPatrolPath() const;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Audio")
	 UAudioComponent* SoundComponent;
	 UPROPERTY(VisibleAnywhere, Category = "Mesh")
	 UStaticMeshComponent* AzurakBody;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

private:
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="AI", meta=(AllowPrivateAccess="true"))
	UBehaviorTree* Tree;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "AI", meta = (AllowPrivateAccess = "true"))
	APatrolPath* PatrolPath;
};
