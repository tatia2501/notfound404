﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "ShowLobbyCodeClass.generated.h"

/**
 * 
 */
UCLASS()
class NOTFOUND404_API UShowLobbyCodeClass : public UUserWidget
{
	GENERATED_BODY()

public:
	UShowLobbyCodeClass(const FObjectInitializer& ObjectInitializer);

protected:
	virtual void NativeConstruct() override;

	UFUNCTION()
	void OnStartGameButtonClicked();

	UPROPERTY(BlueprintReadWrite, meta = (BindWidget))
	class UTextBlock* CodeLabel;

	UPROPERTY(BlueprintReadWrite, meta = (BindWidget))
	class UButton* StartGameButton;
};