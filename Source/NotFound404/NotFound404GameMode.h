// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "NotFound404GameMode.generated.h"

UCLASS(minimalapi)
class ANotFound404GameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	ANotFound404GameMode();
};



