// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/SaveGame.h"
#include "InteractiveObjectsActor.h" 
#include "InventorySave.generated.h"

/**
 * 
 */
UCLASS()
class NOTFOUND404_API UInventorySave : public USaveGame
{
	GENERATED_BODY()
	
public:
    UInventorySave();

    UPROPERTY(VisibleAnywhere, BlueprintReadWrite)
    TArray<AInteractiveObjectsActor*> InteractiveObjectsArray;

    UFUNCTION(BlueprintCallable)
    void AddObject(AInteractiveObjectsActor* NewObject);

    UFUNCTION(BlueprintCallable)
    void RemoveObject(AInteractiveObjectsActor* ObjectToRemove);

    UFUNCTION(BlueprintCallable)
    TArray<AInteractiveObjectsActor*> GetInventoryArray() const;
};
