// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "InteractiveObjectsActor.h"
#include "Logging/LogMacros.h"
#include "InventorySave.h"
#include "PhoneClass.h"
#include "Perception/AIPerceptionStimuliSourceComponent.h"
#include "Perception/AISenseConfig_Sight.h"
#include "Components/SpotLightComponent.h"
#include "NotFound404Character.generated.h"

class UInputComponent;
class USkeletalMeshComponent;
class UCameraComponent;
class UInputAction;
class UInputMappingContext;
struct FInputActionValue;

DECLARE_LOG_CATEGORY_EXTERN(LogTemplateCharacter, Log, All);

UCLASS(config = Game)
class ANotFound404Character : public ACharacter
{
	GENERATED_BODY()

	/** Pawn mesh: 1st person view (arms; seen only by self) */
	UPROPERTY(VisibleDefaultsOnly, Category = Mesh)
	USkeletalMeshComponent* Mesh1P;

	UPROPERTY(VisibleAnywhere, Category = "Mesh")
	UStaticMeshComponent* Object;
	UPROPERTY(VisibleAnywhere, Category = "Inventory")
	USpotLightComponent* LightComponent;
	UPROPERTY(VisibleAnywhere, Category = "Inventory")
	AInteractiveObjectsActor* CurrentObject;

	/** First person camera */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	UCameraComponent* FirstPersonCameraComponent;

	/** MappingContext */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Input, meta = (AllowPrivateAccess = "true"))
	UInputMappingContext* DefaultMappingContext;

	/** Jump Input Action */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Input, meta = (AllowPrivateAccess = "true"))
	UInputAction* JumpAction;

	/** Move Input Action */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Input, meta = (AllowPrivateAccess = "true"))
	UInputAction* MoveAction;

	void Interact();
	void TakeObject();
	void TogglePhone();

	UPROPERTY(EditAnywhere)
	float InteractLineTraceLength = 300.0f;

public:
	ANotFound404Character();
	void CreateSaveFile();
	void LoadGame();
	UFUNCTION(BlueprintCallable, Category = "InventoryFunctions")
	void PutObjectToInventory();
	UFUNCTION(BlueprintCallable, Category = "InventoryFunctions")
	void ShowInventory(bool IsUp);
	UFUNCTION(BlueprintCallable, Category = "InventoryFunctions")
	void HideInventory();
	UFUNCTION(BlueprintCallable, Category = "InventoryFunctions")
	bool IsCharacterInGameLevel();
	UFUNCTION(BlueprintCallable, Category = "InventoryFunctions")
	void ChangeFlashlightLight();

protected:
	virtual void BeginPlay();

public:

	/** Look Input Action */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Input, meta = (AllowPrivateAccess = "true"))
	class UInputAction* LookAction;

	/** Bool for AnimBP to switch to another animation set */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Mesh)
	bool bHasRifle;

	/** Setter to set the bool */
	UFUNCTION(BlueprintCallable, Category = Weapon)
	void SetHasRifle(bool bNewHasRifle);

	/** Getter for the bool */
	UFUNCTION(BlueprintCallable, Category = Weapon)
	bool GetHasRifle();

	void SetFieldOfView();

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite)
	int CurrentObjectNum;

	UPROPERTY(BlueprintReadWrite, EditAnywhere)
	class UInventoryWidget* InventoryWidget;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Widgets")
	TSubclassOf<UInventoryWidget> YourInventoryWidgetClass;
	
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite)
	TArray<AInteractiveObjectsActor*> InventoryArray;
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite)
	int MaxObjectNum;
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite)
	UInventorySave* InventorySave;
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite)
	UInventoryWidget* NewInventoryWidget;
	UPROPERTY(BlueprintReadWrite, EditAnywhere)
	UTexture2D* NoInventoryTexture;
	
	UPROPERTY(EditAnywhere, Category = "Widgets")
	TSubclassOf<UPhoneClass> PhoneWidgetClass;

protected:
	/** Called for movement input */
	void Move(const FInputActionValue& Value);

	/** Called for looking input */
	void Look(const FInputActionValue& Value);

	UPROPERTY(BlueprintReadWrite, EditAnywhere)
	float MouseSensitivity = 0.5f;

	UPROPERTY(BlueprintReadWrite, EditAnywhere)
	int YInvertion = 1;

	UPROPERTY(BlueprintReadWrite, EditAnywhere)
	float FieldOfView = 50;

	UPROPERTY(BlueprintReadWrite, EditAnywhere)
	int CoresNumber;

	UPROPERTY(BlueprintReadWrite, EditAnywhere)
	bool IsObjectInHand = false;

protected:
	// APawn interface
	virtual void SetupPlayerInputComponent(UInputComponent* InputComponent) override;
	// End of APawn interface

public:
	/** Returns Mesh1P subobject **/
	USkeletalMeshComponent* GetMesh1P() const { return Mesh1P; }
	/** Returns FirstPersonCameraComponent subobject **/
	UCameraComponent* GetFirstPersonCameraComponent() const { return FirstPersonCameraComponent; }

private:
	class UAIPerceptionStimuliSourceComponent* StimulusSourse;
	void SetupStimulusSource();
};

