﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "EnterLobbyCodeClass.generated.h"

/**
 * 
 */
UCLASS()
class NOTFOUND404_API UEnterLobbyCodeClass : public UUserWidget
{
	GENERATED_BODY()

public:
	explicit UEnterLobbyCodeClass(const FObjectInitializer& ObjectInitializer);

protected:
	virtual void NativeConstruct() override;
	
	UFUNCTION()
	void OnStartGameButtonClicked();
	
	UFUNCTION()
	void ConnectToServerRequest(int32 Code);
	
	UPROPERTY(BlueprintReadWrite, meta = (BindWidget))
	class UEditableTextBox* CodeInput;

	UPROPERTY(BlueprintReadWrite, meta = (BindWidget))
	class UButton* StartGameButton;
};
