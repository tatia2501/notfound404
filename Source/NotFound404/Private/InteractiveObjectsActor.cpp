// Fill out your copyright notice in the Description page of Project Settings.


#include "InteractiveObjectsActor.h"

AInteractiveObjectsActor::AInteractiveObjectsActor()
{
	Object = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Object"));
	Object->SetupAttachment(RootComponent);
	CollisionComponent = CreateDefaultSubobject<UBoxComponent>(TEXT("CollisionComponent"));
	CollisionComponent->SetupAttachment(Object);
	CollisionComponent->SetCollisionProfileName(TEXT("BlockAllDynamic"));
	IsInInventory = false;
}

void AInteractiveObjectsActor::Attach(UStaticMeshComponent* item_mesh)
{
	item_mesh->SetStaticMesh(Object->GetStaticMesh());
}

void AInteractiveObjectsActor::FreeHand(UStaticMeshComponent* item_mesh)
{
	item_mesh->SetStaticMesh(nullptr);
}
