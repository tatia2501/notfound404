// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/BoxComponent.h"
#include "InteractiveObjectsActor.generated.h"

UCLASS()
class NOTFOUND404_API AInteractiveObjectsActor : public AActor
{
	GENERATED_BODY()
	
public:
	AInteractiveObjectsActor();

	UPROPERTY(VisibleAnywhere, Category = "InteractiveObjectsActor")
	UStaticMeshComponent* Object;
	UPROPERTY(VisibleAnywhere, Category = "InteractiveObjectsActor")
	UBoxComponent* CollisionComponent;
	UPROPERTY(EditAnywhere, Category = "InteractiveObjectsActor")
	UTexture2D* ObjectPhoto;
	UPROPERTY(EditAnywhere, Category = "InteractiveObjectsActor")
	bool IsInInventory;
public:
	virtual void Attach(UStaticMeshComponent* item_mesh);
	void FreeHand(UStaticMeshComponent* item_mesh);
};
