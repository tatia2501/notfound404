// Fill out your copyright notice in the Description page of Project Settings.


#include "GlassBridgeTrigger.h"

AGlassBridgeTrigger::AGlassBridgeTrigger() 
{
	OnActorBeginOverlap.AddDynamic(this, &AGlassBridgeTrigger::Event);
}

void AGlassBridgeTrigger::BeginPlay() 
{
	Super::BeginPlay();
}

void AGlassBridgeTrigger::Event(class AActor* overlappedActor, class AActor* otherActor)
{
    if (otherActor && otherActor != this)
    {
        if (BridgePiece)
        {
            TArray<UStaticMeshComponent*> MeshComponents;
            BridgePiece->GetComponents<UStaticMeshComponent>(MeshComponents);

            for (UStaticMeshComponent* MeshComponent : MeshComponents)
            {
                MeshComponent->SetSimulatePhysics(true);
            }
        }
    }
}