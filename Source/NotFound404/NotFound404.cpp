// Copyright Epic Games, Inc. All Rights Reserved.

#include "NotFound404.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, NotFound404, "NotFound404" );
 