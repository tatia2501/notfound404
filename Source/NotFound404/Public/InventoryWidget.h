// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "Components/Image.h"
#include "InventorySave.h"
#include "InventoryWidget.generated.h"

/**
 * 
 */
UCLASS()
class NOTFOUND404_API UInventoryWidget : public UUserWidget
{
	GENERATED_BODY()

public:
	UInventoryWidget(const FObjectInitializer& ObjectInitializer);

	UPROPERTY(meta = (BindWidget))
	UImage* RightItem;

	UPROPERTY(meta = (BindWidget))
	UImage* LeftItem;

	UPROPERTY(meta = (BindWidget))
	UImage* CentralItem;

	UFUNCTION(BlueprintCallable)
	void SetItems(UTexture2D* LeftItemTexture, UTexture2D* CentralItemTexture, UTexture2D* RightItemTexture);
};
