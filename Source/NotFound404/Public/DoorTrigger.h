// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/TriggerBox.h"
#include "DoorActor.h"
#include "DoorTrigger.generated.h"

/**
 *
 */
UCLASS()
class NOTFOUND404_API ADoorTrigger : public ATriggerBox
{
	GENERATED_BODY()

	ADoorTrigger();

protected:
	virtual void BeginPlay();

public:
	UFUNCTION()
	void Event(class AActor* overlappedActor, class AActor* otherActor);

	UPROPERTY(EditAnywhere, Category = "Actor")
	AActor* Character;

	UPROPERTY(EditAnywhere, Category = "Actor")
	ADoorActor* Door;
};