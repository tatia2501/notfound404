// Fill out your copyright notice in the Description page of Project Settings.


#include "AzurakCharacter.h"

// Sets default values
AAzurakCharacter::AAzurakCharacter()
{
 	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	SoundComponent = CreateDefaultSubobject<UAudioComponent>(TEXT("SoundComponent"));
	SoundComponent->SetupAttachment(RootComponent);
	AzurakBody = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("AzurakBody"));
	AzurakBody->SetupAttachment(RootComponent);
}

// Called when the game starts or when spawned
void AAzurakCharacter::BeginPlay()
{
	Super::BeginPlay();	
}

// Called every frame
void AAzurakCharacter::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

// Called to bind functionality to input
void AAzurakCharacter::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

}

UBehaviorTree* AAzurakCharacter::GetBehavoirTree() const
{
	return Tree;
}

APatrolPath* AAzurakCharacter::GetPatrolPath() const
{
	return PatrolPath;
}

