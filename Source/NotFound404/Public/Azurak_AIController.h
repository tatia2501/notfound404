// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "AIController.h"
#include "Components/AudioComponent.h"
#include "Perception/AIPerceptionTypes.h"
#include "Azurak_AIController.generated.h"

/**
 * 
 */
UCLASS()
class NOTFOUND404_API AAzurak_AIController : public AAIController
{
	GENERATED_BODY()
	
public:
	explicit AAzurak_AIController(FObjectInitializer const& ObjectInitializer);

protected:
	virtual void OnPossess(APawn* InPawn) override;
private:
	class UAISenseConfig_Sight* SightConfig;
	void SetupPerceptionSystem();
	UFUNCTION()
	void OnTargetDetected(AActor* Actor, FAIStimulus const Stimulus);
	UPROPERTY(EditAnywhere, Category = "Audio")
	UAudioComponent* SoundComponent;
	UPROPERTY(VisibleAnywhere, Category = "Mesh")
	UStaticMeshComponent* AzurakBody;
};
