// Fill out your copyright notice in the Description page of Project Settings.


#include "DoorActor.h"
#include "../NotFound404Character.h"

// Sets default values
ADoorActor::ADoorActor()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	Door = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Door"));
	Door->SetupAttachment(RootComponent);
}

// Called when the game starts or when spawned

void ADoorActor::BeginPlay()
{
	Super::BeginPlay();

	static const FString MeshPath = TEXT("/Game/Assets/AbandonedBuilding/DoorRoom1");
	UStaticMesh* DoorMesh = LoadObject<UStaticMesh>(nullptr, *MeshPath);
	if (DoorMesh)
	{
		Door->SetStaticMesh(DoorMesh);
	}
	else
	{
		UE_LOG(LogTemp, Error, TEXT("Failed to load door mesh from path: %s"), *MeshPath);
	}
	
	if (CurveFloat) {
		FOnTimelineFloat TimelineProgress;
		TimelineProgress.BindDynamic(this, &ADoorActor::OpenDoor);
		Timeline.AddInterpFloat(CurveFloat, TimelineProgress);
	}
}

// Called every frame
void ADoorActor::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	Timeline.TickTimeline(DeltaTime);

}

void ADoorActor::OnInteract()
{
	if (bIsDoorClosed) {
		SetDoorOnSameSide();
		Timeline.Play(); // open the door
		bIsDoorClosed = !bIsDoorClosed; // flip flop
		FTimerHandle TimerHandle;
		GetWorld()->GetTimerManager().SetTimer(TimerHandle, [&]()
			{
				Timeline.Reverse();
				bIsDoorClosed = !bIsDoorClosed; // flip flop
			}, 3, false);
	}
}

void ADoorActor::OpenDoor(float Value)
{
	float Angle = bDoorOnSameSide ? DoorRotateAngle : -DoorRotateAngle;
	FRotator Rot = FRotator(0.f, StartYaw + Angle * Value, 0.f);
	Door->SetRelativeRotation(Rot);
}

void ADoorActor::SetDoorOnSameSide()
{
	if (Character) {
		FRotator CurrentRotation = GetActorRotation();
		StartYaw = CurrentRotation.Yaw;
		FVector CharacterFV = Character->GetActorForwardVector();
		FVector DoorFV = GetActorRightVector();
		bDoorOnSameSide = (FVector::DotProduct(CharacterFV, DoorFV) >= 0);
	}
}