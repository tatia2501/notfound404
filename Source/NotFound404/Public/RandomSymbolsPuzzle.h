// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "RandomSymbolSpotActor.h"
#include "RandomSymbolsPuzzle.generated.h"

UCLASS()
class NOTFOUND404_API ARandomSymbolsPuzzle : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ARandomSymbolsPuzzle();

	virtual void BeginPlay();

	void ApplyTextures();
	ARandomSymbolSpotActor* ChooseBigSpot();
	TArray<ARandomSymbolSpotActor*> ChooseSmalSpots();
	void LoadSymbols();
	void HideEmptySpots();
	TArray<UMaterialInterface*> ChooseSymbols();

protected:
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "RandomSymbolsPuzzle")
	TArray<ARandomSymbolSpotActor*> BigSpots;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "RandomSymbolsPuzzle")
	TArray<ARandomSymbolSpotActor*> SmallSpots;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "RandomSymbolsPuzzle")
	TArray<UMaterialInterface*> Materials;
};
