﻿// Fill out your copyright notice in the Description page of Project Settings.


#include "ServerDataSaveGame.h"

UServerDataSaveGame::UServerDataSaveGame()
{
	UserId = "";
	Code = 0;
	ServerId = 0;
}

FString UServerDataSaveGame::GenerateNewId()
{
	const FGuid NewId = FGuid::NewGuid();
	FString Id = NewId.ToString();
	return Id;
}