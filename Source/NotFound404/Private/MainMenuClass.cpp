﻿// Fill out your copyright notice in the Description page of Project Settings.


#include "MainMenuClass.h"

#include "HttpModule.h"
#include "ServerDataSaveGame.h"
#include "Components/Button.h"
#include "Interfaces/IHttpRequest.h"
#include "Interfaces/IHttpResponse.h"
#include "Kismet/GameplayStatics.h"

void UMainMenuClass::OpenShowLobbyCodeWidget()
{
	UShowLobbyCodeClass* ShowLobbyCodeWidget = CreateWidget<UShowLobbyCodeClass>(GetWorld(), ShowLobbyCodeWidgetClass);
	if (ShowLobbyCodeWidget != nullptr)
	{
		ShowLobbyCodeWidget->AddToViewport();
	}
}

void UMainMenuClass::OpenEnterLobbyCodeWidget()
{
	UEnterLobbyCodeClass* EnterLobbyCodeWidget = CreateWidget<UEnterLobbyCodeClass>(GetWorld(), EnterLobbyCodeWidgetClass);
	if (EnterLobbyCodeWidget != nullptr)
	{
		EnterLobbyCodeWidget->AddToViewport();
	}
}

void UMainMenuClass::NativeConstruct()
{
	Super::NativeConstruct();
	if (NewGameButton)
	{
		NewGameButton->OnClicked.AddDynamic(this, &UMainMenuClass::OnNewGameButtonClicked);
	}
	
	if (ConnectToGameButton)
	{
		ConnectToGameButton->OnClicked.AddDynamic(this, &UMainMenuClass::OnConnectToGameButtonClicked);
	}
}

void UMainMenuClass::OnNewGameButtonClicked()
{
	const UServerDataSaveGame* dataToLoad = Cast<UServerDataSaveGame>(UGameplayStatics::LoadGameFromSlot("ServerDataSavedSlot", 0));
	const FString UserId = dataToLoad->UserId;
	const FString PostURL = TEXT("http://localhost:8181/api/servers");
	const FString Content = FString::Printf(TEXT("{ \"creatorId\": \"%s\", \"isPublic\": false }"), *UserId);

	PostServerRequest(PostURL, Content);
}

void UMainMenuClass::OnConnectToGameButtonClicked()
{
	OpenEnterLobbyCodeWidget();
}

void UMainMenuClass::PostServerRequest(const FString& URL, const FString& Content)
{
	FHttpModule* Http = &FHttpModule::Get();
    UE_LOG(LogTemp, Warning, TEXT("POST METHOD"));
    UServerDataSaveGame* dataToLoad = Cast<UServerDataSaveGame>(UGameplayStatics::LoadGameFromSlot("ServerDataSavedSlot", 0));
    dataToLoad->Code = 0;
    UGameplayStatics::SaveGameToSlot(dataToLoad, "ServerDataSavedSlot", 0);
    const TSharedRef<IHttpRequest> Request = Http->CreateRequest();
    Request->SetURL(URL);
    Request->SetVerb("POST");
    Request->SetHeader(TEXT("Content-Type"), TEXT("application/json"));
    Request->SetContentAsString(Content);

    Request->OnProcessRequestComplete().BindLambda([this, dataToLoad](FHttpRequestPtr Request, FHttpResponsePtr Response, bool bSuccess)
    {
        if (bSuccess && Response.IsValid() && (Response->GetResponseCode() == EHttpResponseCodes::Ok || Response->GetResponseCode() == EHttpResponseCodes::Created))
        {
	        const FString ResponseContent = Response->GetContentAsString();
            UE_LOG(LogTemp, Warning, TEXT("Response Content: %s"), *ResponseContent);

            TSharedPtr<FJsonObject> JsonObject;
            TSharedRef<TJsonReader<>> JsonReader = TJsonReaderFactory<>::Create(ResponseContent);
            if (FJsonSerializer::Deserialize(JsonReader, JsonObject))
            {
                int32 Code;
                int32 ServerId;
                if (JsonObject->TryGetNumberField("code", Code) && JsonObject->TryGetNumberField("id", ServerId))
                {
                    dataToLoad->Code = Code;
                    dataToLoad->ServerId = ServerId;
                    UE_LOG(LogTemp, Warning, TEXT("CODE %d"), dataToLoad->Code);
                    UGameplayStatics::SaveGameToSlot(dataToLoad, "ServerDataSavedSlot", 0);
                }
                else
                {
                	dataToLoad->Code = -1;
					dataToLoad->ServerId = 0;
					UGameplayStatics::SaveGameToSlot(dataToLoad, "ServerDataSavedSlot", 0);
					UE_LOG(LogTemp, Error, TEXT("Failed to parse code from JSON"));
                }
            }
            else
            {
            	dataToLoad->Code = -1;
				dataToLoad->ServerId = 0;
				UGameplayStatics::SaveGameToSlot(dataToLoad, "ServerDataSavedSlot", 0);
				UE_LOG(LogTemp, Error, TEXT("Failed to deserialize JSON"));
            }
        }
        else
        {
        	dataToLoad->Code = -1;
			dataToLoad->ServerId = 0;
			UGameplayStatics::SaveGameToSlot(dataToLoad, "ServerDataSavedSlot", 0);
			UE_LOG(LogTemp, Error, TEXT("Failed to process server request"));
        }
    	OpenShowLobbyCodeWidget();
    });

    Request->ProcessRequest();
}
