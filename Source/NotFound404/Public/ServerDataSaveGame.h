﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/SaveGame.h"
#include "ServerDataSaveGame.generated.h"

/**
 * 
 */
UCLASS()
class NOTFOUND404_API UServerDataSaveGame : public USaveGame
{
	GENERATED_BODY()

public:
	UServerDataSaveGame();
	static FString GenerateNewId();

	UPROPERTY(VisibleAnywhere)
	FString UserId;

	UPROPERTY(VisibleAnywhere)
	int32 Code;

	UPROPERTY(VisibleAnywhere)
	int32 ServerId;
};
