// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "InteractiveObjectsActor.h"
#include "Components/SpotLightComponent.h"
#include "FlashLightActor.generated.h"

UCLASS()
class NOTFOUND404_API AFlashLightActor : public AInteractiveObjectsActor
{
	GENERATED_BODY()
	
public:
	AFlashLightActor();

	UPROPERTY(VisibleAnywhere, Category = "FlashLight")
	USpotLightComponent* LightComponent;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "FlashLight")
	bool IsOn;

	UFUNCTION(BlueprintCallable, Category = "FlashLight")
	void ChangeLight();

protected:
	virtual void Attach(UStaticMeshComponent* item_mesh) override;
};
