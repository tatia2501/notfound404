// Fill out your copyright notice in the Description page of Project Settings.


#include "LadderActor.h"
#include "../NotFound404Character.h"
#include "Components/StaticMeshComponent.h"
#include "Components/BoxComponent.h"
#include "GameFramework/CharacterMovementComponent.h"

// Sets default values
ALadderActor::ALadderActor()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

    // ������� ��������� ��������
    Ladder = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Ladder"));
    Ladder->SetupAttachment(RootComponent);
    
    // ������� � ����������� ��������� ���������� ��������
    LadderCollider = CreateDefaultSubobject<UBoxComponent>(TEXT("LadderCollider"));
    if (LadderCollider)
    {
        LadderCollider->SetupAttachment(Ladder); // ����������� LadderCollider � Ladder
    }
    else
    {
        UE_LOG(LogTemp, Error, TEXT("Failed to create LadderCollider component!"));
    }
}

// Called when the game starts or when spawned
void ALadderActor::BeginPlay()
{
    Super::BeginPlay();

}

// Called every frame
void ALadderActor::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}