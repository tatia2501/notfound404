// Fill out your copyright notice in the Description page of Project Settings.


#include "PhoneClass.h"

#include "Components/Button.h"
#include "GameFramework/Character.h"
#include "Kismet/GameplayStatics.h"
#include "NotFound404/NotFound404Character.h"

UPhoneClass::UPhoneClass(const FObjectInitializer& ObjectInitializer) : Super(ObjectInitializer)
{
}

void UPhoneClass::NativeConstruct()
{
	Super::NativeConstruct();
	UE_LOG(LogTemplateCharacter, Error, TEXT("Phone"));

	FInputModeGameAndUI InputMode;
	InputMode.SetWidgetToFocus(this->TakeWidget());
	InputMode.SetLockMouseToViewportBehavior(EMouseLockMode::DoNotLock);
	APlayerController* PlayerController = UGameplayStatics::GetPlayerController(GetWorld(), 0);
	if (PlayerController)
	{
		PlayerController->SetInputMode(InputMode);
		PlayerController->bShowMouseCursor = true;
	}
	
	if (ToOffice)
	{
		ToOffice->OnClicked.AddDynamic(this, &UPhoneClass::OnToOfficeButtonClicked);
	}
	if (ToHall)
	{
		ToHall->OnClicked.AddDynamic(this, &UPhoneClass::OnToHallButtonClicked);
	}
	if (ToAbandoned)
	{
		ToAbandoned->OnClicked.AddDynamic(this, &UPhoneClass::OnToAbandonedButtonClicked);
	}
}

void UPhoneClass::OnToOfficeButtonClicked()
{
	const FVector OfficeLocation(997.0, 671.0, 640.0);
	const FRotator OfficeRotation(0, 0, 100);
	MoveToLocation(OfficeLocation, OfficeRotation);
}

void UPhoneClass::OnToHallButtonClicked()
{
	const FVector HallLocation(-1180.0, 590.0, 176.0);
	const FRotator HallRotation = FRotator::ZeroRotator;
	MoveToLocation(HallLocation, HallRotation);
}

void UPhoneClass::OnToAbandonedButtonClicked()
{
	const FVector AbandonedLocation(-2012, -6485, 120);
	const FRotator AbandonedRotation = FRotator::ZeroRotator;

	MoveToLocation(AbandonedLocation, AbandonedRotation);
}

void UPhoneClass::MoveToLocation(FVector TargetLocation, FRotator TargetRotation)
{
	if (ACharacter* Character = UGameplayStatics::GetPlayerCharacter(GetWorld(), 0))
	{
		Character->SetActorLocationAndRotation(TargetLocation, TargetRotation);

		APlayerController* PlayerController = UGameplayStatics::GetPlayerController(GetWorld(), 0);
		if (PlayerController)
		{
			PlayerController->SetInputMode(FInputModeGameOnly());
			PlayerController->bShowMouseCursor = false;
		}
		this->RemoveFromViewport();
	}
}