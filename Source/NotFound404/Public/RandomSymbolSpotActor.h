// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "RandomSymbolSpotActor.generated.h"

UCLASS()
class NOTFOUND404_API ARandomSymbolSpotActor : public AActor
{
	GENERATED_BODY()
	
public:
	ARandomSymbolSpotActor();

	void ApplyMaterial(UMaterialInterface* Material);

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Spot")
	bool IsBigSpot;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Spot")
	UStaticMeshComponent* SpotMeshComponent;
};
