// Copyright Epic Games, Inc. All Rights Reserved.

using UnrealBuildTool;

public class NotFound404 : ModuleRules
{
	public NotFound404(ReadOnlyTargetRules Target) : base(Target)
	{
		PCHUsage = PCHUsageMode.UseExplicitOrSharedPCHs;

		PublicDependencyModuleNames.AddRange(new string[] { "Core", "CoreUObject", "Engine", "InputCore", "AIModule", "EnhancedInput", "HTTP", "Json", "JsonUtilities", "UMG", "GameplayTasks", "NavigationSystem" });
	}
}
