// Fill out your copyright notice in the Description page of Project Settings.


#include "FlashLightActor.h"

// Sets default values
AFlashLightActor::AFlashLightActor()
{
	LightComponent = CreateDefaultSubobject<USpotLightComponent>(TEXT("LightComponent"));
	LightComponent->SetupAttachment(Object);
	IsOn = false;
	LightComponent->SetVisibility(IsOn);
}

void AFlashLightActor::ChangeLight()
{
	LightComponent->SetVisibility(IsOn = !IsOn);
}

void AFlashLightActor::Attach(UStaticMeshComponent* item_mesh)
{
	Super::Attach(item_mesh);
	item_mesh->SetRelativeRotation(FRotator(0.f, 90.f, 0.f));
}