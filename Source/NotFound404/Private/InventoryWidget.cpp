// Fill out your copyright notice in the Description page of Project Settings.


#include "InventoryWidget.h"
#include "Kismet/GameplayStatics.h"

UInventoryWidget::UInventoryWidget(const FObjectInitializer& ObjectInitializer)
    : Super(ObjectInitializer)
{
}

void UInventoryWidget::SetItems(UTexture2D* LeftItemTexture, UTexture2D* CentralItemTexture, UTexture2D* RightItemTexture)
{
    if (LeftItemTexture == nullptr)
    {
        LeftItem->SetOpacity(0.0f);
    }
    else {
        LeftItem->SetBrushFromTexture(LeftItemTexture);
        LeftItem->SetOpacity(1.0f);
    }
    if (CentralItemTexture == nullptr)
    {
        CentralItem->SetOpacity(0.0f);
    }
    else {
        CentralItem->SetBrushFromTexture(CentralItemTexture);
        CentralItem->SetOpacity(1.0f);
    }
    if (RightItemTexture == nullptr)
    {
        RightItem->SetOpacity(0.0f);
    }
    else {
        RightItem->SetBrushFromTexture(RightItemTexture);
        RightItem->SetOpacity(1.0f);
    }
}
