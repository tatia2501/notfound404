// Fill out your copyright notice in the Description page of Project Settings.


#include "Azurak_AIController.h"
#include "AzurakCharacter.h"
#include "Perception/AIPerceptionComponent.h"
#include "Perception/AISenseConfig_Sight.h"
#include "BehaviorTree/BlackboardComponent.h"
#include "NotFound404/NotFound404Character.h"

AAzurak_AIController::AAzurak_AIController(FObjectInitializer const& ObjectInitializer)
{
	SetupPerceptionSystem();
}

void AAzurak_AIController::OnPossess(APawn* InPawn)
{
	Super::OnPossess(InPawn);
	if (AAzurakCharacter* const azurakCharacter = Cast<AAzurakCharacter>(InPawn))
	{
		if (UBehaviorTree* const tree = azurakCharacter->GetBehavoirTree())
		{
			UBlackboardComponent* b;
			UseBlackboard(tree->BlackboardAsset, b);
			Blackboard = b;
			RunBehaviorTree(tree);
		}
	}
}

void AAzurak_AIController::SetupPerceptionSystem()
{
	SightConfig = CreateDefaultSubobject<UAISenseConfig_Sight>(TEXT("Sight Config"));
	if (SightConfig)
	{
		SetPerceptionComponent(*CreateDefaultSubobject<UAIPerceptionComponent>(
			TEXT("Perception Component")));
		SightConfig->SightRadius = 300.f;
		SightConfig->LoseSightRadius = SightConfig->SightRadius + 100.f;
		SightConfig->PeripheralVisionAngleDegrees = 360.f;
		SightConfig->SetMaxAge(5.f);
		SightConfig->AutoSuccessRangeFromLastSeenLocation = 500.f;
		SightConfig->DetectionByAffiliation.bDetectEnemies = true;
		SightConfig->DetectionByAffiliation.bDetectFriendlies = true;
		SightConfig->DetectionByAffiliation.bDetectNeutrals = true;

		GetPerceptionComponent()->SetDominantSense(*SightConfig->GetSenseImplementation());
		GetPerceptionComponent()->OnTargetPerceptionUpdated.AddDynamic(this, &AAzurak_AIController::OnTargetDetected);
		GetPerceptionComponent()->ConfigureSense((*SightConfig));
	}
}

void AAzurak_AIController::OnTargetDetected(AActor* Actor, FAIStimulus const Stimulus)
{
	if (auto* const ch = Cast<ANotFound404Character>(Actor))
	{
		//GetBlackboardComponent()->SetValueAsBool("CanSeePlayer", Stimulus.WasSuccessfullySensed());
		//GetBlackboardComponent()->SetValueAsBool("TooCloseToPlayer", Stimulus.WasSuccessfullySensed());

		//FVector NPCPosition = GetPawn()->GetActorLocation();
		//FVector PlayerPosition = ch->GetActorLocation();
		//float Distance = FVector::Distance(NPCPosition, PlayerPosition);

		//GetBlackboardComponent()->SetValueAsFloat("PlayerDistance", Distance);
		AAzurakCharacter* AzurakCharacter = Cast<AAzurakCharacter>(GetPawn());

		if (AzurakCharacter)
		{
			SoundComponent = AzurakCharacter->SoundComponent;
			if (SoundComponent)
			{
				SoundComponent->Play();
			}

			AzurakBody = AzurakCharacter->AzurakBody;
			if (AzurakBody)
			{
				FTimerHandle TimerHandle;
				GetWorld()->GetTimerManager().SetTimer(TimerHandle, [&]()
					{
						AzurakBody->SetCastShadow(false);

						GetWorld()->GetTimerManager().SetTimer(TimerHandle, [&]()
							{
								AzurakBody->SetCastShadow(true);
							}, 7, false);
					}, 2, false);
			}
		}
	}
}
