// Copyright Epic Games, Inc. All Rights Reserved.

#include "NotFound404Character.h"
#include "NotFound404Projectile.h"
#include "Animation/AnimInstance.h"
#include "Camera/CameraComponent.h"
#include "Components/CapsuleComponent.h"
#include "Components/SkeletalMeshComponent.h"
#include "EnhancedInputComponent.h"
#include "EnhancedInputSubsystems.h"
#include "InputActionValue.h"
#include "Engine/LocalPlayer.h"
#include "InventoryWidget.h"
#include "FlashLightActor.h"

#include "Misc/EngineVersion.h"
#include "Misc/EngineBuildSettings.h"
#include "GenericPlatform/GenericPlatformMisc.h"
#include <Windows.h>
#include <iostream>
#include "DoorActor.h"
#include "LadderActor.h"
#include "PhoneClass.h"
#include "ServerDataSaveGame.h"
#include "Kismet/GameplayStatics.h"

DEFINE_LOG_CATEGORY(LogTemplateCharacter);

//////////////////////////////////////////////////////////////////////////
// ANotFound404Character

ANotFound404Character::ANotFound404Character()
{
	// Character doesnt have a rifle at start
	bHasRifle = false;

	// Set size for collision capsule
	GetCapsuleComponent()->InitCapsuleSize(55.f, 96.0f);

	// Create a CameraComponent	
	FirstPersonCameraComponent = CreateDefaultSubobject<UCameraComponent>(TEXT("FirstPersonCamera"));
	FirstPersonCameraComponent->SetupAttachment(GetCapsuleComponent());
	FirstPersonCameraComponent->SetRelativeLocation(FVector(-10.f, 0.f, 60.f)); // Position the camera
	FirstPersonCameraComponent->bUsePawnControlRotation = true;
	FirstPersonCameraComponent->SetFieldOfView(FieldOfView);

	// Create a mesh component that will be used when being viewed from a '1st person' view (when controlling this pawn)
	Mesh1P = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("CharacterMesh1P"));
	Mesh1P->SetOnlyOwnerSee(true);
	Mesh1P->SetupAttachment(FirstPersonCameraComponent);
	Mesh1P->bCastDynamicShadow = false;
	Mesh1P->CastShadow = false;
	//Mesh1P->SetRelativeRotation(FRotator(0.9f, -19.19f, 5.2f));
	Mesh1P->SetRelativeLocation(FVector(-30.f, 0.f, -150.f));

	Object = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Object"));
	Object->SetupAttachment(FirstPersonCameraComponent);
	LightComponent = CreateDefaultSubobject<USpotLightComponent>(TEXT("ObjectLight"));
	LightComponent->SetupAttachment(FirstPersonCameraComponent);
	LightComponent->SetVisibility(false);

	SetupStimulusSource();
}

void ANotFound404Character::CreateSaveFile()
{
	UServerDataSaveGame* dataToSave = Cast<UServerDataSaveGame>(UGameplayStatics::CreateSaveGameObject(UServerDataSaveGame::StaticClass()));
	dataToSave->UserId = dataToSave->GenerateNewId();
	FString Id = dataToSave->UserId;
	UE_LOG(LogTemp, Warning, TEXT("Generated Id: %s"), *Id);
	UGameplayStatics::SaveGameToSlot(dataToSave, "ServerDataSavedSlot", 0);
}

void ANotFound404Character::LoadGame()
{
	UServerDataSaveGame* dataToLoad = Cast<UServerDataSaveGame>(UGameplayStatics::LoadGameFromSlot("ServerDataSavedSlot", 0));
	if (dataToLoad != nullptr)
	{
		FString Id = dataToLoad->UserId;
		UE_LOG(LogTemp, Warning, TEXT("Loaded Id: %s"), *Id);
	}
	else if (!UGameplayStatics::DoesSaveGameExist("ServerDataSavedSlot", 0))
	{
		CreateSaveFile();
	}
}

void ANotFound404Character::BeginPlay()
{
	// Call the base class  
	Super::BeginPlay();

	int32 NumAllCores = FGenericPlatformMisc::NumberOfCoresIncludingHyperthreads();
	// UE_LOG(LogTemp, Display, TEXT("Cores: %d"), NumAllCores);
	CoresNumber = NumAllCores;

	CurrentObjectNum = 0;
	InventorySave = Cast<UInventorySave>(UGameplayStatics::LoadGameFromSlot("InventorySaveSlot", 0));
	if (!InventorySave)
	{
		InventorySave = Cast<UInventorySave>(UGameplayStatics::CreateSaveGameObject(UInventorySave::StaticClass()));
	}

	if (!InventorySave)
	{
		UE_LOG(LogTemp, Warning, TEXT("Failed to create or load save game."));
		return;
	}
	InventoryArray = InventorySave->GetInventoryArray();
	MaxObjectNum = InventoryArray.Num();

	FString NoInventoryTexturePath = "/Game/Assets/no_inventory";
	UTexture* FoundTexture = LoadObject<UTexture>(nullptr, *NoInventoryTexturePath);

	if (FoundTexture && FoundTexture->IsA<UTexture2D>())
	{
		NoInventoryTexture = Cast<UTexture2D>(FoundTexture);
	}
	else {
		UE_LOG(LogTemp, Error, TEXT("No NoInventoryTexture!"));
		return;
	}

	NewInventoryWidget = CreateWidget<UInventoryWidget>(GetWorld(), YourInventoryWidgetClass);
	NewInventoryWidget->AddToViewport();
	if (NewInventoryWidget)
	{
		NewInventoryWidget->SetVisibility(ESlateVisibility::Hidden);
	}

	// ��������, ��� ������������ ������
	UGameplayStatics::DeleteGameInSlot("InventorySaveSlot", 0);

	if (UGameplayStatics::DoesSaveGameExist("ServerDataSavedSlot", 0))
	{
		LoadGame();
	}
	else
	{
		CreateSaveFile();
	}

	
	// Add Input Mapping Context
	if (APlayerController* PlayerController = Cast<APlayerController>(Controller))
	{
		if (UEnhancedInputLocalPlayerSubsystem* Subsystem = ULocalPlayer::GetSubsystem<UEnhancedInputLocalPlayerSubsystem>(PlayerController->GetLocalPlayer()))
		{
			Subsystem->AddMappingContext(DefaultMappingContext, 0);
		}
	}

}

//////////////////////////////////////////////////////////////////////////// Input

void ANotFound404Character::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	// Set up action bindings
	if (UEnhancedInputComponent* EnhancedInputComponent = Cast<UEnhancedInputComponent>(PlayerInputComponent))
	{
		// Jumping
		EnhancedInputComponent->BindAction(JumpAction, ETriggerEvent::Started, this, &ACharacter::Jump);
		EnhancedInputComponent->BindAction(JumpAction, ETriggerEvent::Completed, this, &ACharacter::StopJumping);

		// Moving
		EnhancedInputComponent->BindAction(MoveAction, ETriggerEvent::Triggered, this, &ANotFound404Character::Move);

		// Looking
		EnhancedInputComponent->BindAction(LookAction, ETriggerEvent::Triggered, this, &ANotFound404Character::Look);

		// Using
		// EnhancedInputComponent->BindAction(UseAction, ETriggerEvent::Triggered, this, &ANotFound404Character::Use);

		PlayerInputComponent->BindAction("Interact", IE_Pressed, this, &ANotFound404Character::Interact);
		PlayerInputComponent->BindAction("TakeObject", IE_Pressed, this, &ANotFound404Character::TakeObject);
		PlayerInputComponent->BindAction("TogglePhone", IE_Pressed, this, &ANotFound404Character::TogglePhone);
	}
	else
	{
		UE_LOG(LogTemplateCharacter, Error, TEXT("'%s' Failed to find an Enhanced Input Component! This template is built to use the Enhanced Input system. If you intend to use the legacy system, then you will need to update this C++ file."), *GetNameSafe(this));
	}
}

void ANotFound404Character::SetupStimulusSource()
{
	StimulusSourse = CreateDefaultSubobject<UAIPerceptionStimuliSourceComponent>(TEXT("Stimulus"));
	if (StimulusSourse)
	{
		StimulusSourse->RegisterForSense(TSubclassOf<UAISense_Sight>());
		StimulusSourse->RegisterWithPerceptionSystem();
	}
}
void ANotFound404Character::TogglePhone()
{
	FString CurrentLevel = GetWorld()->GetMapName();
	if (CurrentLevel.Equals(TEXT("UEDPIE_0_DetectiveOffice")))
	{
		UPhoneClass* PhoneWidget = CreateWidget<UPhoneClass>(GetWorld(), PhoneWidgetClass);
		if (PhoneWidget != nullptr)
		{
			PhoneWidget->AddToViewport();
		}
	}
}

void ANotFound404Character::Move(const FInputActionValue& Value)
{
	// input is a Vector2D
	FVector2D MovementVector = Value.Get<FVector2D>();

	if (Controller != nullptr)
	{
		// add movement 
		AddMovementInput(GetActorForwardVector(), MovementVector.Y);
		AddMovementInput(GetActorRightVector(), MovementVector.X);
	}
}

void ANotFound404Character::Look(const FInputActionValue& Value)
{
	// input is a Vector2D
	FVector2D LookAxisVector = Value.Get<FVector2D>();

	if (Controller != nullptr)
	{
		// add yaw and pitch input to controller
		AddControllerYawInput(LookAxisVector.X * MouseSensitivity * YInvertion);
		AddControllerPitchInput(LookAxisVector.Y * MouseSensitivity);
	}
	SetFieldOfView();
}

void ANotFound404Character::SetHasRifle(bool bNewHasRifle)
{
	bHasRifle = bNewHasRifle;
}

bool ANotFound404Character::GetHasRifle()
{
	return bHasRifle;
}

void ANotFound404Character::SetFieldOfView()
{
	if (FirstPersonCameraComponent)
	{
		FirstPersonCameraComponent->SetFieldOfView(FieldOfView);
	}
}

void DisplayInfo() {
	DISPLAY_DEVICE Device;
	ZeroMemory(&Device, sizeof(Device));
	Device.cb = sizeof(Device);

	DWORD devNum = 0; 
	DWORD adapterIndex = 0; 

	if (EnumDisplayDevices(NULL, devNum, &Device, 0))
	{
		DEVMODE devMode;
		ZeroMemory(&devMode, sizeof(devMode));
		devMode.dmSize = sizeof(devMode);

		if (EnumDisplaySettings(Device.DeviceName, ENUM_CURRENT_SETTINGS, &devMode))
		{
			// std::cout << "GPU Clock: " << devMode.dmDisplayFrequency << " MHz" << std::endl;
			float frequency = (float)devMode.dmDisplayFrequency;
			// UE_LOG(LogTemp, Display, TEXT("GPU Clock: %f"), frequency);
		}
	}
}

void ANotFound404Character::Interact()
{
	if (FirstPersonCameraComponent == nullptr) return;
	FHitResult HitResult;
	FVector Start = FirstPersonCameraComponent->GetComponentLocation();
	FVector End = Start + FirstPersonCameraComponent->GetForwardVector() * InteractLineTraceLength;
	GetWorld()->LineTraceSingleByChannel(HitResult, Start, End, ECollisionChannel::ECC_Visibility);

	ADoorActor* Door = Cast<ADoorActor>(HitResult.GetActor());
	if (Door) {
		Door->Character = this;
		Door->OnInteract();
	}
}

void ANotFound404Character::TakeObject()
{
	if (FirstPersonCameraComponent == nullptr) return;
	FHitResult HitResult;
	FVector Start = FirstPersonCameraComponent->GetComponentLocation();
	FVector End = Start + FirstPersonCameraComponent->GetForwardVector() * InteractLineTraceLength;
	GetWorld()->LineTraceSingleByChannel(HitResult, Start, End, ECollisionChannel::ECC_Visibility);

	AInteractiveObjectsActor* object = Cast<AInteractiveObjectsActor>(HitResult.GetActor());
	if (object) {
		CurrentObject = object;
		CurrentObject->Attach(Object);
		IsObjectInHand = true;
		object->Destroy();
		if (AFlashLightActor* FlashLight = dynamic_cast<AFlashLightActor*>(CurrentObject))
		{
			LightComponent->SetVisibility(FlashLight->IsOn = true);
		}
	}
}

void ANotFound404Character::ChangeFlashlightLight()
{
	if (AFlashLightActor* FlashLight = dynamic_cast<AFlashLightActor*>(CurrentObject))
	{
		LightComponent->SetVisibility(FlashLight->IsOn = !FlashLight->IsOn);
	}
}

void ANotFound404Character::PutObjectToInventory()
{
	UStaticMeshComponent* MeshComponent = CurrentObject->Object;
	if (MeshComponent)
	{
		InventorySave = Cast<UInventorySave>(UGameplayStatics::LoadGameFromSlot("InventorySaveSlot", 0));

		if (!InventorySave)
		{
			InventorySave = Cast<UInventorySave>(UGameplayStatics::CreateSaveGameObject(UInventorySave::StaticClass()));
		}

		if (!InventorySave)
		{
			UE_LOG(LogTemp, Warning, TEXT("Failed to create or load save game."));
			return;
		}
		
		if (!CurrentObject->IsInInventory) {
			InventorySave->AddObject(CurrentObject);
			CurrentObject->IsInInventory = true;

			if (!UGameplayStatics::SaveGameToSlot(InventorySave, "InventorySaveSlot", 0))
			{
				UE_LOG(LogTemp, Warning, TEXT("Failed to save game."));
			}

			InventoryArray = InventorySave->GetInventoryArray();
			MaxObjectNum = InventoryArray.Num();

			if (AFlashLightActor* FlashLight = dynamic_cast<AFlashLightActor*>(CurrentObject))
			{
				LightComponent->SetVisibility(FlashLight->IsOn = false);
			}

			CurrentObject->FreeHand(Object);
			CurrentObject = nullptr;
			IsObjectInHand = false;
		}
	}
}

void ANotFound404Character::HideInventory()
{
	if (NewInventoryWidget)
	{
		NewInventoryWidget->SetVisibility(ESlateVisibility::Hidden);
	}
}

void ANotFound404Character::ShowInventory(bool IsUp)
{
	if (!InventorySave)
	{
		UE_LOG(LogTemp, Error, TEXT("InventorySave is nullptr!"));
		return;
	}

	if (NewInventoryWidget)
	{
		if (MaxObjectNum <= 0)
		{
			NewInventoryWidget->SetItems(nullptr, NoInventoryTexture, nullptr);
			NewInventoryWidget->SetVisibility(ESlateVisibility::Visible);
			CurrentObject->FreeHand(Object);
			CurrentObject = nullptr;
			IsObjectInHand = false;
		}
		else if (MaxObjectNum == 1 and CurrentObjectNum == 0)
		{
			NewInventoryWidget->SetItems(nullptr, InventoryArray[0]->ObjectPhoto, NoInventoryTexture);
			NewInventoryWidget->SetVisibility(ESlateVisibility::Visible);
			CurrentObject = InventoryArray[0];
			CurrentObject->Attach(Object);
			IsObjectInHand = true;
		}
		else if (CurrentObjectNum == 0)
		{
			NewInventoryWidget->SetItems(nullptr, InventoryArray[0]->ObjectPhoto, InventoryArray[1]->ObjectPhoto);
			NewInventoryWidget->SetVisibility(ESlateVisibility::Visible);
			CurrentObject = InventoryArray[0];
			CurrentObject->Attach(Object);
			IsObjectInHand = true;
		}
		else if (CurrentObjectNum == (MaxObjectNum - 1))
		{
			NewInventoryWidget->SetItems(InventoryArray[MaxObjectNum - 2]->ObjectPhoto, InventoryArray[MaxObjectNum - 1]->ObjectPhoto, NoInventoryTexture);
			NewInventoryWidget->SetVisibility(ESlateVisibility::Visible);
			CurrentObject = InventoryArray[MaxObjectNum - 1];
			CurrentObject->Attach(Object);
			IsObjectInHand = true;
		}
		else if (CurrentObjectNum > 0 and CurrentObjectNum < (MaxObjectNum - 1))
		{
			NewInventoryWidget->SetItems(InventoryArray[CurrentObjectNum - 1]->ObjectPhoto, InventoryArray[CurrentObjectNum]->ObjectPhoto, InventoryArray[CurrentObjectNum + 1]->ObjectPhoto);
			NewInventoryWidget->SetVisibility(ESlateVisibility::Visible);
			CurrentObject = InventoryArray[CurrentObjectNum];
			CurrentObject->Attach(Object);
			IsObjectInHand = true;
		}
		else if (CurrentObjectNum == MaxObjectNum)
		{
			NewInventoryWidget->SetItems(InventoryArray[MaxObjectNum - 1]->ObjectPhoto, NoInventoryTexture, nullptr);
			NewInventoryWidget->SetVisibility(ESlateVisibility::Visible);
			CurrentObject->FreeHand(Object);
			CurrentObject = nullptr;
			IsObjectInHand = false;
		}
		else {
			UE_LOG(LogTemp, Error, TEXT("Inventory size is out of array"));
			return;
		}

		if (CurrentObject)
		{
			if (AFlashLightActor* FlashLight = dynamic_cast<AFlashLightActor*>(CurrentObject))
			{
				LightComponent->SetVisibility(FlashLight->IsOn = true);
			}
			else {
				LightComponent->SetVisibility(false);
			}
		}
		else {
			LightComponent->SetVisibility(false);
		}

		if (IsUp) {
			if (CurrentObjectNum > 0)
			{
				CurrentObjectNum--;
			}
		}
		else {
			if (CurrentObjectNum < MaxObjectNum)
			{
				CurrentObjectNum++;
			}
		}
	}
	else
	{
		UE_LOG(LogTemp, Warning, TEXT("Failed to create InventoryWidget!"));
	}
}

bool ANotFound404Character::IsCharacterInGameLevel()
{
	ULevel* CurrentLevel = GetWorld()->GetCurrentLevel();

	if (CurrentLevel)
	{
		FString LevelName = CurrentLevel->GetOuter()->GetName();
		if (LevelName != "Title" && LevelName != "MainMenu")
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	else
	{
		UE_LOG(LogTemp, Warning, TEXT("Current level is invalid."));
		return false;
	}
}