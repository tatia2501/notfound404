﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "PhoneClass.generated.h"

/**
 * 
 */
UCLASS()
class NOTFOUND404_API UPhoneClass : public UUserWidget
{
	GENERATED_BODY()
public:
	explicit UPhoneClass(const FObjectInitializer& ObjectInitializer);

	virtual void NativeConstruct() override;

	UPROPERTY(BlueprintReadWrite, meta = (BindWidget))
	class UButton* ToOffice;

	UPROPERTY(BlueprintReadWrite, meta = (BindWidget))
	class UButton* ToHall;

	UPROPERTY(BlueprintReadWrite, meta = (BindWidget))
	class UButton* ToAbandoned;

	UFUNCTION()
	void OnToOfficeButtonClicked();
	
	UFUNCTION()
	void OnToHallButtonClicked();
	
	UFUNCTION()
	void OnToAbandonedButtonClicked();
	
	UFUNCTION()
	void MoveToLocation(FVector TargetLocation, FRotator TargetRotation);
};
