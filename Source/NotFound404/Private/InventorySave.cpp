// Fill out your copyright notice in the Description page of Project Settings.


#include "InventorySave.h"

UInventorySave::UInventorySave()
{
}

void UInventorySave::AddObject(AInteractiveObjectsActor* NewObject)
{
    InteractiveObjectsArray.Add(NewObject);
}

void UInventorySave::RemoveObject(AInteractiveObjectsActor* ObjectToRemove)
{
    InteractiveObjectsArray.Remove(ObjectToRemove);
}

TArray<AInteractiveObjectsActor*> UInventorySave::GetInventoryArray() const
{
    return InteractiveObjectsArray;
}