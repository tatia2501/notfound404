// Fill out your copyright notice in the Description page of Project Settings.


#include "RandomSymbolsPuzzle.h"
#include "Kismet/GameplayStatics.h"
#include <algorithm> 
#include <random>

// Sets default values
ARandomSymbolsPuzzle::ARandomSymbolsPuzzle()
{

}

void ARandomSymbolsPuzzle::BeginPlay()
{
	// Call the base class  
	Super::BeginPlay();
	ApplyTextures();
}

void ARandomSymbolsPuzzle::ApplyTextures()
{
	TArray<AActor*> TempCubesInRoom;
	UGameplayStatics::GetAllActorsOfClass(GetWorld(), ARandomSymbolSpotActor::StaticClass(), TempCubesInRoom);

	for (AActor* Actor : TempCubesInRoom)
	{
		ARandomSymbolSpotActor* Spot = Cast<ARandomSymbolSpotActor>(Actor);
		if (Spot->IsBigSpot)
		{
			BigSpots.Add(Spot);
		}
		else {
			SmallSpots.Add(Spot);
		}
	}

	LoadSymbols();
	TArray<UMaterialInterface*> RandomMaterials = ChooseSymbols();
	ARandomSymbolSpotActor* RandomBigSpot = ChooseBigSpot();
	TArray<ARandomSymbolSpotActor*> RandomSmallSpots = ChooseSmalSpots();

	RandomBigSpot->ApplyMaterial(RandomMaterials[0]);

	for (int32 i = 0; i < 4; i++) {
		RandomSmallSpots[i]->ApplyMaterial(RandomMaterials[i+1]);
	}

	HideEmptySpots();
}

ARandomSymbolSpotActor* ARandomSymbolsPuzzle::ChooseBigSpot()
{
	ARandomSymbolSpotActor* RandomBigSpot = nullptr;
	if (BigSpots.Num() > 0)
	{
		int32 RandomIndex = FMath::RandRange(0, BigSpots.Num() - 1);
		RandomBigSpot = BigSpots[RandomIndex];
	}
	return RandomBigSpot;
}

TArray<ARandomSymbolSpotActor*> ARandomSymbolsPuzzle::ChooseSmalSpots()
{
	SmallSpots.Sort([](const ARandomSymbolSpotActor& A, const ARandomSymbolSpotActor& B) {
		return FMath::RandBool();
		});

	TArray<ARandomSymbolSpotActor*> RandomSmallSpots;
	for (int32 i = 0; i < FMath::Min(4, SmallSpots.Num()); ++i)
	{
		RandomSmallSpots.Add(SmallSpots[i]);
	}
	return RandomSmallSpots;
}

void ARandomSymbolsPuzzle::LoadSymbols()
{
	FString MaterialFolderPath = TEXT("/Game/Assets/AbandonedBuilding/numbers/");

	for (int32 MaterialIndex = 2; MaterialIndex <= 20; ++MaterialIndex)
	{
		FString MaterialPath = MaterialFolderPath + FString::Printf(TEXT("%d_Mat"), MaterialIndex);
		UMaterialInterface* NewMaterial = LoadObject<UMaterialInterface>(nullptr, *MaterialPath);

		if (NewMaterial)
		{
			Materials.Add(NewMaterial);
		}
		else
		{
			UE_LOG(LogTemp, Error, TEXT("Failed to load material from path: %s"), *MaterialPath);
		}
	}
}

TArray<UMaterialInterface*> ARandomSymbolsPuzzle::ChooseSymbols()
{
	Materials.Sort([](const UMaterialInterface& A, const UMaterialInterface& B) {
		return FMath::RandBool();
		});

	TArray<UMaterialInterface*> RandomMaterials;

	for (int32 i = 0; i < FMath::Min(5, Materials.Num()); ++i)
	{
		RandomMaterials.Add(Materials[i]);
	}
	return RandomMaterials;
}

void ARandomSymbolsPuzzle::HideEmptySpots()
{
	for (ARandomSymbolSpotActor* BigSpot : BigSpots)
	{
		UStaticMeshComponent* SpotMeshComponent = BigSpot->SpotMeshComponent;
		if (SpotMeshComponent)
		{
			UMaterialInterface* Material = SpotMeshComponent->GetMaterial(0);
			if (Material && Material->IsA(UMaterial::StaticClass()))
			{
				UMaterial* Mat = Cast<UMaterial>(Material);
				if (Mat && Mat->GetFName() == FName("WorldGridMaterial"))
				{
					BigSpot->Destroy();
					//SpotMeshComponent->SetVisibility(false);
				}
			}
		}
	}
	for (ARandomSymbolSpotActor* SmallSpot : SmallSpots)
	{
		UStaticMeshComponent* SpotMeshComponent = SmallSpot->SpotMeshComponent;
		if (SpotMeshComponent)
		{
			UMaterialInterface* Material = SpotMeshComponent->GetMaterial(0);
			if (Material && Material->IsA(UMaterial::StaticClass()))
			{
				UMaterial* Mat = Cast<UMaterial>(Material);
				if (Mat && Mat->GetFName() == FName("WorldGridMaterial"))
				{
					SmallSpot->Destroy();
					//SpotMeshComponent->SetVisibility(false);
				}
			}
		}
	}
}