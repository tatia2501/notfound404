﻿// Fill out your copyright notice in the Description page of Project Settings.


#include "ShowLobbyCodeClass.h"

#include "EnterLobbyCodeClass.h"
#include "ServerDataSaveGame.h"
#include "Components/Button.h"
#include "Components/TextBlock.h"
#include "Kismet/GameplayStatics.h"

UShowLobbyCodeClass::UShowLobbyCodeClass(const FObjectInitializer& ObjectInitializer) : Super(ObjectInitializer)
{
}

void UShowLobbyCodeClass::NativeConstruct()
{
	Super::NativeConstruct();
	const UServerDataSaveGame* dataToLoad = Cast<UServerDataSaveGame>(UGameplayStatics::LoadGameFromSlot("ServerDataSavedSlot", 0));
	if (dataToLoad != nullptr && dataToLoad->Code != -1)
	{
		UE_LOG(LogTemp, Warning, TEXT("CODE LOBBY %d"), dataToLoad->Code);
		CodeLabel->SetText(FText::AsNumber(dataToLoad->Code));

		if (StartGameButton)
		{
			StartGameButton->OnClicked.AddDynamic(this, &UShowLobbyCodeClass::OnStartGameButtonClicked);
		}
	}
	else
	{
		CodeLabel->SetText(FText::FromString("Error..."));
	}

	// Для входа в игру без сервера раскомментить код ниже
	// if (StartGameButton)
	// {
	// 	StartGameButton->OnClicked.AddDynamic(this, &UShowLobbyCodeClass::OnStartGameButtonClicked);
	// }
}

void UShowLobbyCodeClass::OnStartGameButtonClicked()
{
	FString LevelName = TEXT("DetectiveOffice");
	UGameplayStatics::OpenLevel(GetWorld(), FName(*LevelName));
}